
'use strict';

const fs = require('fs');
const util = require('util');
const path = require('path');
const aws = require('aws-sdk');
const jsdump = require('jsDump');

/* Singleton AWS objects */
let ec2, firehose, iam, kinesis, lambda, logs, redshift, s3, sts;

/* Async wrappers */
const fs_stat = util.promisify(fs.stat);
const fs_readdir = util.promisify(fs.readdir);
const fs_readfile = util.promisify(fs.readFile);

/**
 * @name create_aws_global_objects:
 */
const create_aws_global_objects = async (_region) => {

  aws.config.update({ region: _region });

  s3 = new aws.S3();
  ec2 = new aws.EC2();
  sts = new aws.STS();
  iam = new aws.IAM();
  lambda = new aws.Lambda();
  kinesis = new aws.Kinesis();
  firehose = new aws.Firehose();
  redshift = new aws.Redshift();
  logs = new aws.CloudWatchLogs();
};

/**
 * @name sleep:
 *   Wait for some time to let a resource settle if there's
 *   no other possible way to do it. I'm looking at you IAM.
 */
const sleep = async (_ms) => {

  return new Promise((_resolve_fn) => {
    setTimeout(() => _resolve_fn(_ms), _ms);
  });
};

/**
 * @name regexp_escape:
 *   Implement a RegExp method that should really already exist.
 */
const regexp_escape = (_regexp_str) => {

  /* https://stackoverflow.com/questions/3561493 */
  return _regexp_str.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
};

/**
 * @name log:
 *   A deadly simple async section-based logging function.
 */
const log = async (_message, _level, _fn) => {

  const timestamp = (new Date()).toISOString();
  const level = (_level || 'info').toLowerCase();

  if (level === 'fatal') {
    process.stderr.write("\n");
  }

  process.stderr.write(`[${timestamp}] ${level}: ${_message}`);

  if (_fn) {
    process.stderr.write(`... `);
    await _fn(_message, level);
    process.stderr.write(`done.\n`);
    return Promise.resolve();
  }

  process.stderr.write("\n");
};

/**
 * @name template:
 *   A tiny templating engine for rewriting AWS JSON configuration.
 *   Just enough to get the job done; intentionally minimalist. A
 *   production environment would use a radically more sophisticated
 *   tool and/or approach, and would either operate on a real JSON
 *   parse tree or understand the JSON string escaping rules. For
 *   purposes of this exercise, I'm treating AWS API-originating
 *   as trusted. Would not in production.
 */
const template = (_json_str, _map) => {

  let s = _json_str;
  let map = (_map || {});

  for (var k in map) {
    s = s.replace(
      new RegExp(`%${regexp_escape(k.toUpperCase())}%`, 'g'), map[k]
    );
  }

  return s;
};

/**
 * @name deploy:
 *   For each file in `_dir` with a file extension of `_file_type`,
 *   invoke the async function `_fn(content, file_basename, file_path)`.
 *   This function itself returns a promise and can be used with `await`.
 *   If `_file_type` is not specified, it defaults to `json`.
 */
const deploy = async (_dir, _fn, _file_type, _use_buffer) => {

  const files = await fs_readdir(_dir);
  const file_type = (_file_type || 'json');

  for (var i = 0, len = files.length; i < len; ++i) {

    const file_name = files[i];

    if (!file_name.endsWith(`.${file_type}`)) {
      continue;
    }

    const file_path = path.join(_dir, file_name);
    const file_basename = path.basename(file_path, `.${file_type}`);
    const stat = await fs_stat(file_path);

    if (stat.isFile()) {
      const file_content = await fs_readfile(file_path);
      const r = (_use_buffer ? file_content : file_content.toString());
      await _fn(r, file_basename, file_path);
    }
  }
};

/**
 * @name get_account_identifier:
 */
const get_account_identifier = async () => {

  const r = await sts.getCallerIdentity().promise();
  return r.Account;
};

/**
 * @name autoselect_s3_bucket
 */
const autoselect_s3_bucket = (_rv) => {

  /* There can be only one (for now) */
  const k = (Object.keys(_rv.s3.bucket)[0]);
  return _rv.s3.bucket[k];
};

/**
 * @name discover_default_vpc:
 */
const discover_default_vpc = async (_rv, _cfg) => {

  const v = await ec2.describeVpcs({
    Filters: [{
      Name: 'isDefault',
      Values: [ 'true' ]
    }]
  }).promise();

  _rv.vpc.default = v.Vpcs[0]
};

/**
 * @name discover_default_subnets:
 */
const discover_default_subnets = async (_rv, _cfg) => {

  const s = await ec2.describeSubnets({
    Filters: [{
      Name: 'vpc-id',
      Values: [ _rv.vpc.default.VpcId ]
    }]
  }).promise();

  s.Subnets.forEach((_s) => {
    _rv.vpc.subnet[_s.SubnetId] = _s;
  });
};

/**
 * @name create_cloudwatch_log_groups:
 */
const create_cloudwatch_log_groups = async (_rv, _cfg) => {

  return deploy(_cfg.cloudwatch.log_group_dir, async (_json, _name) => {
    _rv.logs.group[_name] = await logs.createLogGroup(
      JSON.parse(template(_json, _cfg.cloudwatch))
    ).promise();
  });
};

/**
 * @name create_cloudwatch_log_streams:
 */
const create_cloudwatch_log_streams = async (_rv, _cfg) => {

  return deploy(_cfg.cloudwatch.log_stream_dir, async (_json, _name) => {
    _rv.logs.stream[_name] = await logs.createLogStream(
      JSON.parse(template(_json, _cfg.cloudwatch))
    ).promise();
  });
};

/**
 * @name add_iam_policies:
 */
const add_iam_policies = async (_rv, _cfg) => {

  const s3_bucket = autoselect_s3_bucket(_rv);

  /* Discover AWS and S3 details */
  _cfg.iam.region = _cfg.region;
  _cfg.iam.account_id = _cfg.account_id;
  _cfg.iam.s3_bucket_arn = s3_bucket.Arn;

  return deploy(_cfg.iam.policy_dir, async (_json, _name) => {
    _rv.iam.policy[_name] = await iam.createPolicy({
      PolicyName: _name,
      PolicyDocument: template(_json, _cfg.iam)
    }).promise();
  });
};

/**
 * @name add_iam_roles:
 */
const add_iam_roles = async (_rv, _cfg) => {

  return deploy(_cfg.iam.trust_dir, async (_json, _name) => {

    /* Add initial role */
    _rv.iam.role[_name] = await iam.createRole({
      RoleName: _name,
      AssumeRolePolicyDocument: template(_json, _cfg)
    }).promise();

    /* Bind policy to role */
    _rv.iam.attachment[_name] = await iam.attachRolePolicy({
      RoleName: _name,
      PolicyArn: _rv.iam.policy[_name].Policy.Arn
    }).promise();

    /* Make roles available to EC2 by default */
    _rv.iam.profile[_name] = await iam.createInstanceProfile({
      InstanceProfileName: _name
    }).promise();

    /* Either throws or goes; no return value */
    await iam.addRoleToInstanceProfile({
      InstanceProfileName: _name, RoleName: _name
    }).promise();
  });
};

/**
 * @name create_security_groups:
 */
const create_security_groups = async (_rv, _cfg) => {

  /* Determine default VPC identifier */
  _cfg.security.vpc_id = _rv.vpc.default.VpcId;

  return deploy(_cfg.security.group_dir, async (_json, _name) => {
    const g = _rv.security.group[_name] = await ec2.createSecurityGroup(
      JSON.parse(template(_json, _cfg.security))
    ).promise();
    await ec2.createTags({
      Resources: [ g.GroupId ],
      Tags: [{ Key: 'Name', Value: _name }]
    }).promise();
  });
};

/**
 * @name create_security_group_rules:
 */
const create_security_group_rules = async (_rv, _cfg) => {

  _cfg.security.group = {};

  /* Copy in security group identifiers */
  for (var name in _rv.security.group) {
    const c = _cfg.security.group[name] = {};
    c.security_group_id = _rv.security.group[name].GroupId;
  }

  /* Ingress (required regardless of VPC status) */
  await deploy(_cfg.security.ingress_dir, async (_json, _name) => {
    _rv.security.ingress[_name] = await ec2.authorizeSecurityGroupIngress(
      JSON.parse(template(_json, _cfg.security.group[_name]))
    ).promise();
  });

  /* Egress (non-default VPC only, currently unused) */
  return deploy(_cfg.security.egress_dir, async (_json, _name) => {
    _rv.security.egress[_name] = await ec2.authorizeSecurityGroupEgress(
      JSON.parse(template(_json, _cfg.security.group[_name]))
    ).promise();
  });
};

/**
 * @name create_redshift_clusters:
 */
const create_redshift_clusters = async (_rv, _cfg) => {

  /* Determine ARN for `turbopump` IAM role */
  _cfg.redshift.iam_role_arn = _rv.iam.role.turbopump.Role.Arn;

  /* Determine identifier for `thruster` security group */
  _cfg.redshift.security_group_id = _rv.security.group.thruster.GroupId;

  return deploy(_cfg.redshift.cluster_dir, async (_json, _name) => {
    _rv.redshift[_name] = await redshift.createCluster(
      JSON.parse(template(_json, _cfg.redshift))
    ).promise();
  });
};

/**
 * @name wait_for_redshift_clusters:
 */
const wait_for_redshift_clusters = async (_rv, _cfg) => {

  for (var name in _rv.redshift) {
    const c = _rv.redshift[name];

    /* Wait for network connectivity */
    const x = await redshift.waitFor('clusterAvailable', {
      ClusterIdentifier: c.Cluster.ClusterIdentifier
    }).promise();

    /* Retrieve updated metadata, including hostname */
    const cluster_metadata = await redshift.describeClusters({
      ClusterIdentifier: c.Cluster.ClusterIdentifier
    }).promise();

    /* Update cluster information */
    c.Cluster = cluster_metadata.Clusters[0];
  }
};

/**
 * @name create_kinesis_streams:
 */
const create_kinesis_streams = async (_rv, _cfg) => {

  return deploy(_cfg.kinesis.stream_dir, async (_json, _name) => {

    const obj = JSON.parse(template(_json, _cfg.kinesis));
    await kinesis.createStream(obj).promise();

    /* Wait for asynchronous creation */
    await kinesis.waitFor('streamExists', {
      StreamName: obj.StreamName
    }).promise();

    /* Retrieve updated metadata */
    _rv.kinesis.stream[_name] = await kinesis.describeStreamSummary({
      StreamName: obj.StreamName
    }).promise();
  });
};

/**
 * @name create_s3_buckets:
 */
const create_s3_buckets = async (_rv, _cfg) => {

  return deploy(_cfg.s3.bucket_dir, async (_json, _name) => {

    const obj = JSON.parse(template(_json, _cfg));
    const bucket_name = `${obj.Bucket || _name}`;
    const result = await s3.createBucket(obj).promise();

    /* Add additional identifiers */
    _rv.s3.bucket[_name] = Object.assign(result, {
      Name: bucket_name,
      Arn: `arn:aws:s3:::${bucket_name}`
    });
  });
};

/**
 * @name upload_lambda_packages:
 */
const upload_lambda_packages = async (_rv, _cfg) => {

  return deploy(_cfg.lambda.package_dir, async (_zip_data, _name) => {

    const s3_bucket = autoselect_s3_bucket(_rv);
    const s3_bucket_name = s3_bucket.Name;
    const s3_bucket_url = s3_bucket.Location;
    const s3_object_key = `lambda/${_name}.zip`;

    const result = await s3.putObject({
      Body: _zip_data,
      Key: s3_object_key,
      Bucket: s3_bucket_name,
      StorageClass: 'REDUCED_REDUNDANCY'
    }).promise();

    _rv.lambda.s3[_name] = Object.assign(result, {
      Key: s3_object_key,
      Bucket: s3_bucket_name,
      Location: `${s3_bucket_url}${s3_object_key}`
    });
  }, 'zip', true);
};

/**
 * @name create_lambda_functions:
 */
const create_lambda_functions = async (_rv, _cfg) => {

  _cfg.lambda.fn = {};

  return deploy(_cfg.lambda.fn_dir, async (_json, _name) => {

    /* Copy in per-function config information */
    const c = _cfg.lambda.fn[_name] = Object.assign({}, _cfg.redshift);

    /* Determine ARN for `gridfin` IAM role */
    c.iam_role_arn = _rv.iam.role.gridfin.Role.Arn;

    /* Determine ARN for `gridfin` IAM role */
    c.redshift_iam_role_arn = _rv.iam.role.turbopump.Role.Arn;

    /* Determine S3 package bucket information */
    c.s3_key = _rv.lambda.s3[_name].Key;
    c.s3_bucket = _rv.lambda.s3[_name].Bucket;

    /* Determine VPC */
    c.vpc_id = _rv.vpc.default.VpcId;

    /* Determine identifier for security group */
    c.security_group_id = _rv.security.group.thruster.GroupId;

    /* Determine subnet identifiers */
    c.subnet_ids = JSON.stringify(Object.keys(_rv.vpc.subnet));

    /* Determine hostname */
    c.host = _rv.redshift.falcon.Cluster.Endpoint.Address;

    _rv.lambda.fn[_name] = await lambda.createFunction(
      JSON.parse(template(_json, c))
    ).promise();
  });
};

/**
 * @name create_s3_notification_configuration:
 */
const create_s3_notification_configuration = async (_rv, _cfg) => {

  _cfg.s3.notify = {};
  const s3_bucket = autoselect_s3_bucket(_rv);

  return deploy(_cfg.s3.notify_dir, async (_json, _name) => {

    /* Create per-notification config */
    const c = _cfg.s3.notify[_name] = {};
    c.s3_lambda_bucket = _rv.lambda.s3[_name].Bucket;
    c.s3_lambda_arn = _rv.lambda.fn[_name].FunctionArn;

    /* Add permission */
    _rv.lambda.permission[_name] = await lambda.addPermission({
      StatementId: _name,
      FunctionName: _name,
      SourceArn: s3_bucket.Arn,
      Principal: 's3.amazonaws.com',
      SourceAccount: _cfg.account_id,
      Action: 'lambda:InvokeFunction'
    }).promise();

    /* Add notification */
    _rv.s3.notify[_name] = await s3.putBucketNotificationConfiguration(
      JSON.parse(template(_json, c))
    ).promise();
  });
};

/**
 * @name create_kinesis_delivery_streams:
 */
const create_kinesis_delivery_streams = async (_rv, _cfg) => {

  const s3_bucket = autoselect_s3_bucket(_rv);
  const lambda_fn_name = 'apache-json-cfl-log-transformer';

  /* Discover required ARNs */
  _cfg.kinesis.s3_bucket_arn = s3_bucket.Arn;
  _cfg.kinesis.iam_role_arn = _rv.iam.role.raptor.Role.Arn;
  _cfg.kinesis.lambda_arn = _rv.lambda.fn[lambda_fn_name].FunctionArn;

  _cfg.kinesis.kinesis_stream_arn = (
    _rv.kinesis.stream.merlin.StreamDescriptionSummary.StreamARN
  );

  return deploy(_cfg.kinesis.firehose_dir, async (_json, _name) => {
    _rv.kinesis.firehose[_name] = await firehose.createDeliveryStream(
      JSON.parse(template(_json, _cfg.kinesis))
    ).promise();
  });
};

/**
 * @name run:
 *   This is intentionally low-level code; I'm scripting this deploy
 *   without the use of orchestration tools to hopefully demonstrate
 *   some level of first-principles understanding. In production,
 *   one would almost certainly use a higher-level approach to automation,
 *   but this method is relatively self-contained and has the benefit of
 *   requiring a minimum of dependencies/tooling to run properly.
 */
const run = async (_cfg) => {

  let exit_status = 0;

  let rv = {
    redshift: {},
    s3: { bucket: {}, notify: {} },
    logs: { group: {}, stream: {} },
    vpc: { default: {}, subnet: {} },
    security: { group: {}, ingress: {} },
    kinesis: { stream: {}, firehose: {} },
    lambda: { s3: {}, fn: {}, permission: {} },
    iam: { attachment: {}, profile: {}, policy: {}, role: {} }
  };

  /* Create AWS singleton instances */
  create_aws_global_objects(_cfg.region);

  /* Discover account identifier */
  _cfg.account_id = await get_account_identifier();

  /* Deploy:
      Deploy the entire AWS stack. This could certainly use
      Cloudformation or another higher-level tool, but I wanted
      to spend time with the AWS SDK directly and learn it from
      first principles. It's around 0.4 kSLOC; not too terrible. */

  try {

    await log('Creating S3 buckets', null, async () => {
      await create_s3_buckets(rv, _cfg);
    });

    await log('Discovering default VPC', null, async () => {
      await discover_default_vpc(rv, _cfg);
    });

    await log('Discovering subnets for default VPC', null, async () => {
      await discover_default_subnets(rv, _cfg);
    });

    await log('Creating Cloudwatch log groups', null, async () => {
      await create_cloudwatch_log_groups(rv, _cfg);
    });

    await log('Creating Cloudwatch log streams', null, async () => {
      await create_cloudwatch_log_streams(rv, _cfg);
    });

    await log('Creating security groups', null, async () => {
      await create_security_groups(rv, _cfg);
    });

    await log('Waiting for security groups to settle', null, async () => {
      await sleep(1 << 12); /* "Usable almost immediately" */
    });

    await log('Creating security group rules', null, async () => {
      await create_security_group_rules(rv, _cfg);
    });

    await log('Adding IAM policies', null, async () => {
      await add_iam_policies(rv, _cfg);
    });

    await log('Waiting for IAM to settle', null, async () => {
      await sleep(1 << 14); /* "Usable almost immediately" */
    });

    await log('Adding IAM roles', null, async () => {
      await add_iam_roles(rv, _cfg);
    });

    await log('Starting Redshift cluster creation', null, async () => {
      await create_redshift_clusters(rv, _cfg);
    });

    await log('Creating Kinesis streams', null, async () => {
      await create_kinesis_streams(rv, _cfg);
    });

    await log('Uploading Lambda packages', null, async () => {
      await upload_lambda_packages(rv, _cfg);
    });

    await log('Waiting for Redshift clusters', null, async () => {
      await wait_for_redshift_clusters(rv, _cfg);
    });

    await log('Creating Lambda functions', null, async () => {
      await create_lambda_functions(rv, _cfg);
    });

    await log('Creating S3 notification configuration', null, async () => {
      await create_s3_notification_configuration(rv, _cfg);
    });

    await log('Creating Kinesis delivery streams', null, async () => {
      await create_kinesis_delivery_streams(rv, _cfg);
    });

    log('Deployment completed successfully');

  } catch (_e) {

    log('Failed to deploy one or more resources to AWS', 'fatal');
    process.stderr.write(`Backtrace: ${_e.stack}\n`);
    exit_status = 127;
  }

  /* Emit full results regardless */
  process.stdout.write(jsdump.parse({
    results: rv, config: _cfg
  }));

  process.stdout.write("\n");
  process.exit(exit_status);
};

/**
 * @name main:
 */
const main = async (_argv) => {

  try {
    await run(JSON.parse(
      await fs_readfile(path.join(__dirname, '..', 'config.json'))
    ));
  } catch (_e) {
    console.error(_e.stack);
    process.exit(127);
  }
};

/* Go */
main(process.argv);

