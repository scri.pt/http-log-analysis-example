#!/bin/bash

main()
{
  for file in "$@"; do
    perl -pe 's/\n//;' < "$file" | jq -r '
      .results.redshift.falcon.Cluster.Endpoint.Address +
        ":" + (.results.redshift.falcon.Cluster.Endpoint.Port | tostring) +
        ":" + .config.redshift.user + ":" + .config.redshift.database +
        ":" + .config.redshift.password
    '
  done
}

main "$@"

