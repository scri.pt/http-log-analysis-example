#!/bin/bash

usage()
{
  echo "Usage: $0 ~/.pgpass" >&2
  exit 1
}

main()
{
  local pg_pass_file="$1"
  shift 1

  [ -f "$pg_pass_file" ] || usage

  # Overly-simplistic parsing
  local host="`cut -d: -f1 < "$pg_pass_file"`" &&
  local port="`cut -d: -f2 < "$pg_pass_file"`" &&
  local user="`cut -d: -f3 < "$pg_pass_file"`" &&
  local database="`cut -d: -f4 < "$pg_pass_file"`" &&
  local password="`cut -d: -f5 < "$pg_pass_file"`" &&
  \
  # Version 8.x, no PGPASSFILE
  PGPASSWORD="$password" \
    exec psql "$@" -h "$host" -p "$port" -U "$user" "$database"
  
}

main "$@"

