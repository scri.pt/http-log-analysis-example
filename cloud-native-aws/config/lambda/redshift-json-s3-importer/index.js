
/* redshift-json-s3-importer:
 *
 *   Issue RDS or Redshift `COPY` commands in response to the delivery
 *   of Kinesis JSON segments to S3. I've taken this approach because the
 *   Kinesis Firehose S3 delivery method produces lots of files with
 *   unpredictable UUIDs embedded in their names, yet the Kinesis Firehose
 *   Redshift delivery method forces a `MANIFEST` option into the `COPY`
 *   command, making it (apparently) unusable in situations where you
 *   need to ingest entire directories or can't predict and exhaustively
 *   enumerate S3 key/file names in advance.
 */

'use strict';

const { Client } = require('pg');

/**
 * @name copy_command:
 *   A SQL template for copying JSON data from S3.
 */
const copy_command = `
  copy http_access_logs from $1 
    iam_role $2
    format as json 'auto'
    dateformat 'YYYY-MM-DD HH24:MI:SS';
`;

/**
 * @name prepare:
 *   A tiny parameterized query implementation, since the
 *   built-in node-postgres one isn't working in this case.
 *   The built-in implementation appears to rely on an actual
 *   connection-level `parse` operation, and I suspect that
 *   something's going wrong in that area, either due to a
 *   libpq issue/mismatch or due to Redshift modifications.
 *   Just do it ourself and avoid the issue entirely; the
 *   values arguably come from trusted network sources anyway.
 */
const prepare = (_sql, _values) => {

  let rv = _sql;

  for (var i = 0, len = _values.length; i < len; ++i) {

    const v = _values[i];

    rv = rv.replace(`$${i + 1}`, () => {
      return "'" + v.replace('\\', '\\\\').replace("'", "\\'") + "'";
    });
  }

  return rv;
};

/**
 * @name handler:
 */
exports.handler = async (_ev, _ctx, _cb) => {

  const pg = new Client();
  const records = (_ev.Records || []);
  const iam_role_arn = process.env.IAM_ROLE_ARN;

  if (!iam_role_arn) {
    console.error('[WARN] Required environment variable missing');
    return;
  }

  if (records.length <= 0) {
    console.error('[INFO] Function invoked with nothing to do');
    return;
  }

  /* To do:
      This should cached outside of `handler` or via `_ctx`. */

  await pg.connect();

  try {

    for (var i = 0, len = records.length; i < len; ++i) {

      const r = records[i]; 
      const s3_key = r.s3.object.key;
      const s3_bucket = r.s3.bucket.name;
      const s3_url = `s3://${s3_bucket}/${s3_key}`;

      await pg.query(prepare(copy_command, [ s3_url, iam_role_arn ]));
    }

  } catch (_e) {

    console.error(`[FAIL] ${_e.message}`);
    console.error(`[FAIL] ${_e.stack}`);
  }

  await pg.end();
};

