
/* Drop existing objects ignoring errors */
drop table if exists http_access_logs cascade;

/**
 * @name schema:
 */
begin transaction;

/**
 * @name http_access_logs:
 */
create table http_access_logs (
  host varchar(1024) not null,
  ident varchar(64) null,
  authuser varchar(64) null,
  datetime varchar(64) not null,
  request varchar(4096) not null,
  response integer null,
  bytes integer null,
  method varchar(32) null,
  protocol varchar(32) null,
  path_query varchar(4096) null,
  user_agent varchar(2048) null,
  referrer_url varchar(4096) null,
  processed_at timestamp not null,
  timestamp_utc timestamp not null,
  timestamp_utc_offset integer not null
) sortkey (timestamp_utc, response);

/**
 * @name top_ten_failed_endpoints:
 *   Assignment question number one.
 */
create view top_ten_failed_endpoints as
  select
    path_query as path_query, count(path_query) as count
  from
    http_access_logs
  where
    response <> 200
  group by
    path_query
  order by
    count desc limit 10;

/**
 * @name unique_host_count:
 *   Assignment question number two.
 */
create view unique_host_count as
  select
    count(distinct host) as count
  from
    http_access_logs;

/**
 * @name unique_hosts_by_day:
 *   Assignment question number three.
 */
create view unique_hosts_by_day as
  select
    cast(u.date as date) as date_utc,
    count(u.unique_daily_host) as unique_hosts
  from (
    select distinct
      host as unique_daily_host,
      date_trunc('day', timestamp_utc) as date
    from
      http_access_logs
  ) as u
  group by
    u.date
  order by
    u.date asc;

commit transaction;
vacuum;

