
FROM amazonlinux:latest
MAINTAINER hello@scri.pt

ADD contrib/amazon-kinesis-agent /usr/src/aws-kinesis-agent

RUN \
  yum-config-manager --enable epel && \
  yum update -y && yum install -y \
    ant curl inotify-tools java jq perl postgresql sudo which wget

ADD kinesis-agent/agent.json /etc/aws-kinesis/

RUN \
  cd /usr/src/aws-kinesis-agent && \
  sudo ./setup --install && mkdir -p /opt/scripts

ADD incoming/logs /var/log/
ADD docker/scripts /opt/scripts/

ENTRYPOINT [ "/opt/scripts/docker-init.sh" ]

