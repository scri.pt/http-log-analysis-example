#!/usr/bin/perl

# Extract JSON parse errors from aws-kinesis-agent logs:
#   This script uses a fairly inefficient backtracking regular expression to
#   find data processing/conversion warnings raised by the AWS Kinesis Agent.
#   Use this if you're ingesting logs that don't quite conform to the Apache
#   Common Log Format specification (e.g. non-RFC-compliant hostnames, missing
#   URL encoding) and you need to discover information about the content of
#   those requests. This definitely isn't a production solution for monitoring
#   parse failures in the Kinesis Agent - just a quick data discovery tool.
#
#   Caution: this appears to require that a TTY be allocated for it to work
#   in containerized environments. I've added it to the Docker image to
#   clean up the output a bit; make sure to use the `docker run -t`.

while (<>) {

  if (/.*?(Agent:\s+Startup\s+completed\s+.+)$/) {
    print "[BOOT] $1\n"; next;
  }

  if (/.*?Agent:\s+Progress:\s+(.+)$/) {
    print "[INFO] $1\n"; next;
  }

  if (/
    \.LogToJSONDataConverter\s+\[WARN\]     # Match log class
    (?:.*?)\[                               # Anything until [
    (.*?)                                   # Then anything until...
    \]\s*,\s+record\ will\ be\ skipped\s+$  # ...the known string suffix
  /x) {
    print "[DENY] Invalid upstream input: '$1'\n"; next;
  }
}

