#!/bin/bash

fatal()
{
  echo "[FAIL] $*" >&2;
  exit 127;
}

wait_for_files_in_directory()
{
  local dir="$1"
  local file_glob="$2"
  local max_seconds="$3"
  shift 3

  if [ -z "$max_seconds" ]; then
    max_seconds=30
  fi

  local i=0

  while [ "$i" -lt "$max_seconds" ]; do
    local n="`find "$dir" -type f -name "$file_glob" | wc -l`";
    [ "$n" -gt 0 ] && break
    i="$(($i + 1))"; sleep 1
  done
}

main()
{
  /usr/bin/start-aws-kinesis-agent &
  kinesis_agent_pid="$!"

  log_directory=/var/log/aws-kinesis-agent
  wait_for_files_in_directory "$log_directory" '*.log' \
    || fatal 'Kinesis agent failed to produce log output'

  # Suppress inotify-unsupported message from tail
  tail -n 1048576 -f "$log_directory"/*.log 2>/dev/null \
    | /opt/scripts/detect-kinesis-agent-parse-errors.pl &

  tail_pid="$!"
  wait "$kinesis_agent_pid"
  kill -KILL "$tail_pid"
  exit "$?"
}

main "$@"

