
const fs = require('fs')
const _ = require('lodash');
const moment = require('moment');
const jsdump = require('jsDump');
const readline = require('readline');

/**
 * @name apache_clf_regex:
 *   This is an extremely permissive 1995-compliant regular expression
 *   that can extract individual fields from the Apache common log format.
 */
const apache_clf_regex = (
  /^([^\s]+)\s+([^\s]+)\s+([^\s]+)\s+\[([^\]]+)\]\s+"([^"]+)"\s+(\d+)\s+([-\d]+)$/
);

/**
 * @name http_request_regex:
 *   This is another extremely-permissive regular expression that
 *   extracts the HTTP method, unvalidated URL endpoint, and unvalidated
 *   HTTP protocol string (c.f. some 1995 servers accepted `HTTP/V1.0`).
 *   This expression is a backtracking NFA and we could probably do better.
 */
const http_request_regex = (
  /^([^\s]+)\s+(.+?)(\s+[\w\d\.\/]+)?$/
);

/**
 * @name parse_apache_clf:
 *   Parse a line from an Apache common log format file and
 *   return an key/value pair object describing the parsed entry.
 *   If the line can't be (permissively) parsed, throw the line.
 */
const parse_apache_clf = (_line) => {

  const rv = {};

  /* Parse log entry */
  let m = _line.trim().match(apache_clf_regex);

  if (!m) {
    throw new Error(_line);
  }

  let fields = [
    'host', 'ident', 'authuser',
    'datetime', 'request', 'response', 'bytes'
  ];

  fields.forEach((_key, _i) => {
    rv[_key] = m[_i + 1];
  });

  /* Parse request */
  m = rv.request.trim().match(http_request_regex);

  fields = [
    'method', 'path_query', 'protocol'
  ];

  fields.forEach((_key, _i) => {
    rv[_key] = (m ? m[_i + 1] : null);
  });

  /* Parse date */
  try {
    rv.timestamp_utc = moment(rv.datetime, 'DD/MMM/YYYY:HH:mm:ss Z').utc();
  } catch (_e) {
    throw new Error(_line);
  }

  return rv;
};

/**
 * increment_key:
 *   Increment `_obj[_k]` by one, creating the key if necessary.
 */
const increment_key = (_obj, _k) => {

  _obj[_k] = (_obj[_k] ? _obj[_k] : 0) + 1;
};

/**
 * compute_top_failed_endpoints:
 *   Solution to analytics assignment question two.
 */
const compute_top_failed_endpoints = (_array_map, _n) => {

  let rv = [];
  let agg = _.invertBy(_array_map);
  let keys = Object.keys(agg).sort((_a, _b) => (_b - _a));

  for (var i = 0, ln1 = keys.length; i < ln1; ++i) {

    const endpoints = agg[keys[i]];

    for (var j = 0, ln2 = endpoints.length; j < ln2; ++j) {
      rv.push([ endpoints[j], keys[i] ]);

      if (--_n <= 0) {
        /* N reached */
        return rv;
      }
    }
  }

  /* Fewer than N */
  return rv;
};

/**
 * compute_unique_hosts_by_day:
 *   Solution to analytics assignment question three.
 */
const compute_unique_hosts_by_day = (_double_map) => {

  const rv = [];

  _.each(_double_map, (_host_map, _date) => {
    return rv.push([ _date, Object.keys(_host_map).length ]);
  });

  return rv.sort((_a, _b) => (_b <= _a ? 1 : -1));
};

/**
 * @name main:
 */
const main = (_argv) => {

  let unique_hosts = {};
  let fail_endpoints = {};
  let unique_hosts_by_day = {};

  let total_skipped = 0;
  let total_processed = 0;

  /* Stdin */
  const reader = readline.createInterface({
    terminal: false,
    input: process.stdin,
    output: process.stdout
  });

  /* For each line... */
  reader.on('line', (_l) => {

    try {
      const l = parse_apache_clf(_l);

      /* Question one */
      unique_hosts[l.host] = true;

      /* Question two */
      if (l.response != 200) { /* Type abuse */
        increment_key(fail_endpoints, l.path_query);
      }

      /* Question three */
      let date = l.timestamp_utc.startOf('day').format('YYYY-MM-DD');

      if (!unique_hosts_by_day[date]) {
        unique_hosts_by_day[date] = {};
      }
      
      unique_hosts_by_day[date][l.host] = true;

      /* Finished */
      total_processed++;

    } catch (_e) {

      /* 32 Problems */
      total_skipped++;
      console.warn(`[DENY] Invalid upstream input: '${_e.message}'`);
    }
  });

  /* Completion handler */
  reader.on('close', () => {

    process.stdout.write(jsdump.parse({
      total_skipped: total_skipped,
      total_processed: total_processed,
      unique_host_count: Object.keys(unique_hosts).length,
      unique_hosts_by_day: compute_unique_hosts_by_day(unique_hosts_by_day),
      top_failed_endpoints: compute_top_failed_endpoints(fail_endpoints, 10)
      
    }));
    process.stdout.write("\n");
  });
};

main(process.argv);

