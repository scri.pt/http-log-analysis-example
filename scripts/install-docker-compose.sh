#!/bin/bash

fatal()
{
  echo "[FAIL] $*" >&2;
  exit 127;
}

if [ "`id -u`" -ne 0 ]; then
  fatal 'This script must run as root; godspeed'
fi

shopt -s xpg_echo
echo 'Installing docker-compose:' >&2

m="`uname -s`-`uname -m`" &&
path='/usr/local/bin/docker-compose' &&
url="https://github.com/docker/compose/releases/download/1.21.2/docker-compose-$m" &&
\
# Living very, very dangerously here
curl -L# "$url" -o "$path" && chmod +x "$path" \
  || fatal 'Installation of docker-compose failed'

