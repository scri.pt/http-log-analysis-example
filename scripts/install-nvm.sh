#!/bin/bash

fatal()
{
  echo "[FAIL] $*" >&2;
  exit 127;
}

shopt -s xpg_echo
echo 'Installing nvm:' >&2

# Living pretty dangerously here
url='https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh'
unset NVM_DIR && curl -L# -o- "$url" | bash &>/dev/null

if [ ${PIPESTATUS[0]} -ne 0 -o ${PIPESTATUS[1]} -ne 0 ]; then
  fatal 'Installation of NVM failed'
fi

echo "
  export NVM_DIR=\"\$HOME/.nvm\";
  [ -s \"\$NVM_DIR/nvm.sh\" ] && . \"\$NVM_DIR/nvm.sh\";
  [ -s \"\$NVM_DIR/bash_completion\" ] && . \"\$NVM_DIR/bash_completion\";
  true;
"

