
### Definitions ###

TS := date '+%Y-%m-%dT%H:%M:%S.000Z'
NASA_LOG_OUTPUT_FILE := '19950701-19950831.log'

NASA_LOG_URLS := 'ftp://ita.ee.lbl.gov/traces/NASA_access_log_Jul95.gz' \
  'ftp://ita.ee.lbl.gov/traces/NASA_access_log_Aug95.gz'

ifeq (${SUDO},)
  DOCKER_COMPOSE := docker-compose
else
  DOCKER_COMPOSE := sudo /usr/local/bin/docker-compose
endif


### Public Targets ###

all: fetch-if-necessary kinesis-agent-internal docker-internal cloud-deploy

test: test-e2e
install-dependencies: amazon-linux-dependencies

cloud-deploy:
	@cd cloud-native-aws && ${MAKE}

schema:
	@cd cloud-native-aws && ${MAKE} schema

docker: fetch-if-necessary kinesis-agent docker-internal
kinesis-agent: clean-kinesis-agent kinesis-agent-internal

fetch: fetch-logs
fetch-if-necessary: fetch-logs-if-necessary
fetch-logs: fetch-logs-begin fetch-nasa-logs fetch-logs-finish

repatch: unpatch patch
patch: patch-kinesis-agent
unpatch: unpatch-kinesis-agent
distclean: clean clean-kinesis-agent distclean-recursive

clean: unpatch clean-recursive clean-logs
	@rm -f ./output/*/*
	@rm -rf ./estes/node_modules
	@rm -f ./incoming/logs/finished

clean-logs: clean-nasa-logs


### Private Targets ###

test-e2e: test-estes

test-estes: fetch-if-necessary
	@echo "[`${TS}`] info: Running estes end-to-end test" >&2 && \
	cd estes && npm install --silent >/dev/null && \
	node index.js < '../incoming/logs/nasa/${NASA_LOG_OUTPUT_FILE}' \
	  2> ../output/estes/error.log && \
	echo "[`${TS}`] info: Compare to production, check output/estes" >&2 && \
	echo "[`${TS}`] info: Estes end-to-end test is complete" >&2

clean-kinesis-agent:
	@rm -rf ./contrib/amazon-kinesis-agent

clean-recursive:
	@cd cloud-native-aws && ${MAKE} clean

amazon-linux-dependencies:
	@sudo yum install -y docker patch jq pv
	@sudo ./scripts/install-docker-compose.sh
	@eval "`./scripts/install-nvm.sh`" && nvm install 8.10 && nvm use 8.10

distclean-recursive:
	@cd cloud-native-aws && ${MAKE} distclean

docker-internal:
	@echo "[`${TS}`] info: Building Docker image" >&2 && \
	dir=output/docker && file="$$dir/`${TS}`.log" && \
	rm -f "$$dir/latest.log" && touch "$$file" && \
	ln -sf "`basename "$$file"`" "$$dir/latest.log" && \
	${DOCKER_COMPOSE} build 2>&1 \
	  | pv -ptl -w 79 -s 900 > "$$file" && \
	[ $${PIPESTATUS[0]} -eq 0 ] && \
	echo "[`${TS}`] info: Docker image built successfully" >&2

clean-nasa-logs:
	@rm -rf ./incoming/logs/nasa

fetch-logs-if-necessary:
	@if [ ! -f ./incoming/logs/finished ]; then \
	  ${MAKE} clean-logs fetch-logs; \
	fi

fetch-logs-begin:
	@echo "[`${TS}`] info: Fetching HTTP access logs" >&2

fetch-nasa-logs: clean-nasa-logs
	@dir=./incoming/logs/nasa && \
	file='${NASA_LOG_OUTPUT_FILE}' && \
	mkdir -p "$$dir" && cd "$$dir" && \
	for url in ${NASA_LOG_URLS}; do \
	  curl '-#' "$$url" | gunzip -c >> "$$file"; \
	  [ "$$?" -eq 0 ] || exit 127; \
	done

fetch-logs-finish:
	@touch ./incoming/logs/finished

kinesis-agent-internal: fetch-kinesis-agent patch

fetch-kinesis-agent: clean-kinesis-agent
	@echo "[`${TS}`] info: Downloading AWS Kinesis Agent" >&2 && \
	cd contrib && \
	git clone -q 'https://github.com/awslabs/amazon-kinesis-agent.git' && \
	echo "[`${TS}`] info: AWS Kinesis Agent downloaded successfully" >&2

patch-kinesis-agent:
	@cd contrib/amazon-kinesis-agent && \
	for diff in ../../patches/*.diff; do \
	  patch -sf -r ../../output/patch/rejected -p1 < "$$diff"; \
	done

unpatch-kinesis-agent:
	@dir=contrib/amazon-kinesis-agent && \
	[ -d "$$dir" ] || exit 0; cd "$$dir" && \
	for diff in ../../patches/*.diff; do \
	  file="`basename $$diff`" && \
	  patch -sf -r ../../output/patch/reject -p1 -R < "$$diff" &>/dev/null \
	    || echo "[`${TS}`] info: Unpatch ignored for '$$file'"; \
	done


