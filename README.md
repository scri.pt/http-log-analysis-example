Hello, World!
=============

Applicant
---------

David Brown <[hello@scri.pt](mailto:hello@scri.pt)>  
Portland, Oregon, United States

Introduction
------------

This is a solution to a time-limited cloud native log analytics pipeline
assignment that I took on for an interview in 2018. There are two completely
independent implementations in this repository designed to mutually test one
another:

  1. The [primary implementation](https://bit.ly/2TBldAb) is a
     highly-scalable cloud-native pipeline built with IAM, Kinesis Streams,
     Kinesis Firehose, S3, Lambda, and Amazon Redshift. Logs are ingested,
     parsed, and emitted as JSON using a [modified](https://bit.ly/2YmXA1X)
     version of the Amazon Kinesis Agent, running in a hand-built Docker
     container.  Buffer sizes and timeouts are configurable via Kinesis
     Firehose, allowing in-production latency/cost tradeoffs. The Kinesis
     stream is a sharded service capable of ingesting up to 500 MiB/sec at
     default account limits, and much higher rates via an AWS service limit
     increase. Amazon Redshift is a multi-node clustered columnar-storage
     petabyte-scale database with a modified PostgreSQL 8.x frontend, and
     can be scaled up or down while in production. The entire deployment
     process is [automated](https://bit.ly/2OrCQS5) using the bare AWS SDK
     on Node.js.

  2. The [secondary implementation](https://bit.ly/2UYFEZo) is a
     simple ES6 script for Node.js that shares no code with the primary
     implementation. It is considerably faster for this workload, but would
     not scale into the terabyte or petabyte range. It was used as an second
     independently-implemented cross-check - an end-to-end test of sorts.

If you'd like, you can also examine the [answers](https://bit.ly/2WsuA7g)
and [deployment log](https://bit.ly/2FDtYpO) first, without downloading
or running any code. [Screenshots](https://bit.ly/2TxWahj) showing some
of the performance monitoring and visualization tools are also available.


Deployment Instructions
-----------------------

Launch a 64-bit Amazon Linux HVM image (e.g. `ami-e251209a`) om EC2, and allow
the instance to assume a role bearing [this policy](https://bit.ly/2Ub9tc4)
(or greater). You can achieve this by attaching a server role to the instance,
by authenticating using STS, or by using an IAM account's access key(s).
If you are using a root AWS account, no action is required. We'll be deploying
from within AWS, as the Redshift database will not be publicly accessible.

Run the following commands to install the required build-time tools.

```shell
sudo yum install -y git postgresql pv &&
git clone 'https://gitlab.com/scri.pt/http-log-analysis-example.git' &&
cd http-log-analysis-example &&
make install-dependencies &&
sudo service docker start &&
logout
```

Logging out and back in (or invoking another shell) allows the Node.js
version manager (`nvm`) to set up the shell environment properly.

Currently, the git repository ships with a default target region of
`us-west-2`. It's quite possible that this is not what you want. If
you'd like use a different region, perform the following steps:

  - Choose a region other than `us-east-1` region until issue #1 is resolved.
    This region is treated as a special case by the AWS API; it was their first.
  - Run `cd ~/http-log-analysis-example/kinesis-agent`
  - Edit [agent.json](https://bit.ly/2TCC99w), replacing any references to
    `us-west-2` with the region you've selected.
  - Run `cd ~/http-log-analysis-example/cloud-native-aws`
  - Modify the `.Region` property in [config.json](https://bit.ly/2I13U9J)

The above region-handling process is obviously not ideal, and can be
improved in a future version. The need to explicitly configure endpoint
URLs in the Kinesis Agent's configuration file appears to be an upstream bug.

Now, build and deploy the application. This will deploy the cloud-native
analytics pipeline, as well as produce a Docker image that can feed
logs into the pipeline with a single `docker run` command. If your
Docker system is set up to allow you unprivileged (read: non-root)
access, you may omit the `SUDO=y` prefix.

```shell
cd ~/http-log-analysis-example &&
SUDO=y make -s
```

Once the system is deployed and the Docker image is built, deploy the
database schema to Redshift and start the Docker container. The 1995
NASA logs will begin streaming to S3 and then in to Amazon Redshift.
A terminal allocation (i.e. the `-t` option) is required.

```shell
make -s schema &&
sudo docker run -t scri.pt/kinesis-streamer
```

To connect to Redshift and examine the tables and views directly,
run the following command. You can do this at any time while the
Docker container is running, and watch queries as records stream in.

```shell
cd cloud-native-aws &&
./scripts/psql-wrapper.sh output/postgresql/.pgpass
```

Finally, to run a completely independent ES6 implementation of this
exercise that shares no code with the above solution, run the following
command. These results have been [cross-checked](https://bit.ly/2Ubkz0v)
with the primary implementation as an end-to-end test. The results are
identical.

```shell
make -s test
```

You can find information about the results of a build - including logs
showing any reasons for failure, in [these](https://bit.ly/2OvMt2g)
[two](https://bit.ly/2Wk8lQC) directories.


Data Quality Analysis
---------------------

Initial analysis revealed a total of 539 records that violate current
RFCs by including now-invalid characters in hostnames (e.g. `slip/ppp`).
After modifying the Amazon Kinesis Agent parser to party like it was
1995, only 32 malformed log entries remained. I chose to reject and
alert on these: all contained bare quotation marks in the request which
were not properly URL-encoded by the HTTP server software; today this
would likely be indicative of a software fault somewhere upstream.
Visions of pagers going off.


Assignment Interpretation
-------------------------

Both solutions interpret time zones correctly (even though all log
entries are UTC -0400), and store values internally as UTC (Zulu).
For the third question, I interpreted "day" as if I lived in London
(i.e. used UTC days). However, the original UTC offset information
is stored in Kinesis and in Redshift, and can be used to trivially
modify the Redshift PostgreSQL [query](https://bit.ly/2JHbRD6)
to return days from the view of any time zone.

Due to the intentional rejection of 32 ambiguously-malformed log
entries, query results may differ slightly from other implementations.
Use `docker logs` to see these logged alerts.


Design Tradeoffs and Rationale
------------------------------

I chose this technical architecture for the following reasons, not
necessarily in this order:

  1. I wanted to create a pipeline capable of processing high-volume
     streams of data in a massively-parallel environment.

  2. I wanted to provide multiple integration points for other teams.
     For simple analysis, access to timestamped newline-delimited
     JSON can be granted via S3. For near real-time applications, the
     Kinesis stream can be consumed directly, or piped directly into
     Splunk or Kinesis Analytics. For ad-hoc queries, Redshift
     provides a reasonably-familiar SQL interface with fairly fast
     execution time - a middle ground. Other non-SQL database systems
     could be fed by the Kinesis publish/subscribe stream if needed.

  3. I wanted to learn about a bunch of stuff I'd never touched
     before in a hands-on way: Kinesis, Firehose, Lambda, Redshift,
     the AWS SDK, and the gory details of the underlying AWS APIs.

  4. I chose a Lambda function to ingest data from S3 into Redshift
     because the current AWS-provided integration appears broken.
     Specifically, it forcibly adds a `MANIFEST` clause to the
     Redshift `COPY` statement, which makes it unusable for S3
     buckets containing unpredictable file names. There was no
     documented way I could find to remove the `MANIFEST` clause
     or provide my own `COPY` statement in full.

  5. I deliberately avoided orchestration tools and higher-level
     automation frameworks in order to gain first-principles
     knowledge of AWS services and APIs. Some design and tool
     choices were made in order to demonstrate technical range,
     and would not be done in this way in production.

  6. The Amazon Kinesis Agent currently pushes the log parsing
     and JSON-encoding workload out to log publishers. This
     may not be ideal depending upon the use case, but the
     tool supports multiple streams and local files, and
     supports local checkpointing and log rotation.


Future Directions
-----------------

  1. Permissions and security hardening. Some IAM roles could
     have privileges reduced without any functional consequence.
     Additional limited-privilege Redshift users and roles should
     be set up instead of relying on the default administrative
     user account.

  2. The cloud-native solution could use some JSON fuzzing and
     improved unit testing. I've included two sample Lambda
     test documents, but a more finely-grained testing framework
     (with additional unit tests) would be required in production.
     I took the sledgehammer approach to testing this time, and
     wrote a totally separate second clean-room implementation.

  3. I've managed to acquire 200GiB of gzipped and de-identified
     server logs from [Wikipedia](https://wikipedia.org). With
     some minor (though long-running) data massaging, these files
     would provide an excellent large-scale test (and I really
     want to see how fast this thing could go).

  4. All logs related to the log processing itself are either
     emitted via `docker logs` or sent to Cloudwatch; the latter
     can get kind of noisy, especially when invoking Lambda functions.

  5. Pick up an orchestration tool or two and do a similar
     exercise with less development time.

  6. Directly subscribe to and consume the Kinesis stream to power
     real-time updating visualizations. I have experience doing
     front-end work like this, but am unfortunately out of time.


Authorship Statement
--------------------

Unless explicitly noted inline, all work in this repository is my own.
Extra care was taken to not copy AWS-provided examples. Outside of
`package.json` dependencies, only one line was taken from the public
domain - specifically, a well-known regular expression for escaping
characters that are being included in a regular expression.


Just One More Thing
-------------------

[This video](https://www.youtube.com/watch?v=abI6nk0GPNU) is the
product of a live-coding talk I gave about a year ago. I couldn't
help it: NASA web server logs clearly also demand self-landing rockets.

Thanks in advance for taking the time to read/review my submission.
This was a blast; I had a lot of fun.

